Simple setup for postCSS with gulp

To start run npm install, after that run gulp watch and open index.html from /client/index.html

```js
$ npm install
$ gulp watch
open index.html
```

for more gulp tasks check gulpfile.js