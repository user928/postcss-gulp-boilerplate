var gulp = require('gulp');
var sourcemaps = require('gulp-sourcemaps');
var changed = require('gulp-changed');
var replace = require('gulp-replace');
var htmlmin = require('gulp-htmlmin');
var imagemin = require('gulp-imagemin');

// project
var todo = require('gulp-todo');

// css
var postcss = require('gulp-postcss');
var cssnext = require('postcss-cssnext');
var assets = require('postcss-assets');
var nested = require('postcss-nested');
var mixins = require('postcss-mixins');
var postcssImport = require('postcss-import');
var pxtorem = require('postcss-pxtorem');
var minify = require('gulp-minify-css');

gulp.task('copy-js', function () {
    return gulp.src('client/js/**/*')
        .pipe(changed('public/js'))
        .pipe(gulp.dest('public/js'));
});

// optimize images using imagemin
gulp.task('images', function () {
    return gulp.src('client/img/**/*')
        .pipe(changed('public/img'))
        .pipe(imagemin({ optimizationLevel: 3, progressive: true, interlaced: true }))
        .pipe(gulp.dest('public/img'));
});

// postcss compailer with plugins
gulp.task('css', ['images', 'copy-js'], function () {
    var processors = [
        postcssImport({
            plugins: [
                mixins(),
                nested()
            ]
        }),
        cssnext({ browsers: 'last 2 version, > 1%, ie >= 10, iOS >= 7', url: false }),
        assets({ basePath: 'public/', relativeTo: 'public/css', loadPaths: ['img'], cachebuster: true }),
        pxtorem()
    ];

    return gulp.src(['client/css/style.css'])
        .pipe(sourcemaps.init())
        .pipe(postcss(processors))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('public/css/'))
});

// minify css
gulp.task('minify', ['css'], function () {
    return gulp.src('public/css/*.css')
        .pipe(htmlmin({collapseWhitespace: true, removeComments : true}))
        .pipe(gulp.dest('public/css/'));
});

// minify html
gulp.task('minifyhtml', function() {
  return gulp.src('client/*.html')
    .pipe(htmlmin({collapseWhitespace: true, }))
    .pipe(gulp.dest('public'));
});

gulp.task('watch', ['css', 'copy-js'], function () {
    gulp.watch(['client/css/**/*.css', 'client/img/**/*', 'client/js/**/*'], ['css']);
});

// get all todos from code
gulp.task('todos', function () {
    return gulp.src(['client/**/*.css', 'client/**/*.js', '**/*.html'])
        .pipe(todo())
        .pipe(gulp.dest('./'));
});

// gulp replace path for css and js files in public folder (this is dirty solution)
gulp.task('replacePath', function() {
    return gulp.src('./public/index.html')
        .pipe(replace('../public/', ''))
        .pipe(gulp.dest('./public'));
});